module Main where

import Network.HTTP.Simple
import qualified Data.ByteString.Char8 as B8

main :: IO ()
main = do
  req <- parseRequest "https://haskell.org"
  res <- httpBS req
  B8.putStrLn (getResponseBody res)

